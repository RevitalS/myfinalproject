﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project1.Models;
using Project1.Utility;
using Project1.Constants;

namespace Project1.Controllers
{
    /// <summary>
    /// The inctructions and training.
    /// </summary>
    public class InstructionsController : Controller
    {

        public ActionResult Instructions()
        {
            return View();
        }
        [HttpGet]
        public ActionResult PictureInstructions()
        {
            TimeSessionManager.StartInstructionsTimer();
            ExperimentSessionManager.SetState(ExperimentState.Instructions);
            ViewBag.Instructions = GetInstructionsString();
            return View();
        }

        [HttpGet]
        public ActionResult PictureInstructions2()
        {
            TimeSessionManager.StopInstructionsTimer();
            TimeSessionManager.StartTrainingTimer();
            return View();
        }

        [HttpPost]
        public ActionResult PictureInstructions2(ChosenPictures chosenPictures)
        {
            if (chosenPictures.ChosenImage != null)
            {
                TimeSessionManager.StopTrainingTimer();

                return RedirectToAction("Quiz", "Quiz");
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Training()
        {
            if (ExperimentSessionManager.GetState().Equals(ExperimentState.Instructions))
            {
                TimeSessionManager.StopInstructionsTimer();
                TimeSessionManager.StartTrainingTimer();
                ExperimentSessionManager.SetState(ExperimentState.Training);
            } else if (ExperimentSessionManager.GetState() > ExperimentState.Training)
            {
                return RedirectToAction("PictureInstructions", "Instructions");
            }
            var model = new ChosenPictures();
            model.NumberOfPic = 3;
            return View(model);
        }

        [HttpPost]
        public ActionResult Training(ChosenPictures model)
        {
            if (model.ChosenImage != null)
            {
                model.NumberOfPic = 3;
                model.DidChoose = true;
                return View(model);
                //TimeSessionManager.StopTrainingTimer();
                //return RedirectToAction("Quiz", "Quiz");
            }
            else
            {
                var m = new ChosenPictures();
                m.NumberOfPic = 3;
                m.DidChoose = false;
                return View(m);
            }
        }

        public ActionResult GoToQuiz()
        {
            TimeSessionManager.StopTrainingTimer();
            return RedirectToAction("Quiz", "Quiz");
        }

        [HttpGet]
        public ActionResult ExamplePicture(string val)
        {
            //PictureSessionManager.SetMethod(ExperimentMethods.MoralPicture);
            ViewBag.Massage = val;
            ViewBag.Src = Url.Content("~/pic/examplePicture" + val+".jpg");


            /* if (PictureSessionManager.GetMethod() != ExperimentMethods.MoralPicture)
             {
                 switch (val)
                 {
                     case "1":
                         ViewBag.Src = Url.Content("~/pic/" + PicturesHolder.catExample1);
                         break;
                     case "2":
                         ViewBag.Src = Url.Content("~/pic/" + PicturesHolder.catExample2);
                         break;
                     case "3":
                         ViewBag.Src = Url.Content("~/pic/" + PicturesHolder.catExample3);
                         break;
                     default:
                         break;
                 }
             }
             else
             {
                 switch (val)
                 {
                     case "1":
                         ViewBag.Src = Url.Content("~/pic/" + PicturesHolder.moralExample1);
                         break;
                     case "2":
                         ViewBag.Src = Url.Content("~/pic/" + PicturesHolder.moralExample2);
                         break;
                     case "3":
                         ViewBag.Src = Url.Content("~/pic/" + PicturesHolder.moralExample3);
                         break;
                     default:
                         break;
                 }
             }*/
            return View();
        }

        [HttpGet]
        public ActionResult CountClick(string val)
        {
            return RedirectToAction("Training");
        }

        private string GetInstructionsString()
        {
            var s = "In this game you need to look at " + GameConstants.NumberOfPictures.ToString() +
                        " images of umbrellas and choose the one you believe is the strongest.\n" +
                        "You will be provided " + GameConstants.NumberOfPictures.ToString() + " links.\n" +
                        "each leading to different umbrella.\n" +
                        "Once you watched all " + GameConstants.NumberOfPictures +
                        " umbrellas you will need to choose the best one in your opinion.";
                        
            
            return s;
        }

    }

}
