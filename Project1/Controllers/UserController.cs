﻿using Project1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project1.Utility;
using Project1.Constants;

namespace Project1.Controllers
{
    /// <summary>
    /// Save all the user start details and the profile in the DB
    /// </summary>
    public class UserController : Controller
    {
        [HttpGet]
        public ActionResult UserProfile()
        {
            TimeSessionManager.StartUserProfileTimer();
            //cencel back and set the current state
            if (ExperimentSessionManager.GetState() > ExperimentState.PersonalInfo)
            {
                return RedirectToAction("PictureInstructions", "Instructions");
            }
            ExperimentSessionManager.SetState(ExperimentState.PersonalInfo);

            var use = new PicturesUsers();
            use.Genders = getGendersList();
            use.Educations = getEducationsList();
            use.Countries = getCountriesList();
            use.Ages = getAgesList();
            return View(use);
        }
        [HttpPost]
        public ActionResult UserProfile(PicturesUsers model)
        {
            if (FlagsSessionManager.GetUserFlag() == null || FlagsSessionManager.GetUserFlag() == false)
            {
                FlagsSessionManager.SetUserFlag(true);
            }
            if (ModelState.IsValid)
            {
                if (AMTSessionManager.GetWorkerId() != "friend1")
                {
                    using (var db = new MainDbContext())
                    {

                        var picUser = db.PicturesUsers.Create();
                        picUser.WorkerId = AMTSessionManager.GetWorkerId();
                        picUser.HitId = AMTSessionManager.GetHitId();
                        picUser.AssignmentId = AMTSessionManager.GetAssignmentId();
                        picUser.Age = model.Age;
                        picUser.Gender = model.Gender;
                        picUser.Country = model.Country;
                        picUser.Education = model.Education;
                        picUser.StartTime = TimeSessionManager.GetStartTime();
                        // picUser.Order = PictureSessionManager.GetOrder().ToString();
                        picUser.Method = PictureSessionManager.GetMethod().ToString();
                        db.PicturesUsers.Add(picUser);
                        db.SaveChanges();
                    }
                }
                TimeSessionManager.StopUserProfileTimer();
                return RedirectToAction("Methods", "Methods");
               // return RedirectToAction("StartExp");
            }
            else
            {
                model.Ages = getAgesList();
                model.Educations = getEducationsList();
                model.Genders = getGendersList();
                model.Countries = getCountriesList();
            }
            return View(model);
        }

        private List<SelectListItem> getAgesList()
        {
            var ageList = new List<SelectListItem>()
            {
                new SelectListItem() {Value = "18", Text = "18" },
                new SelectListItem() {Value = "19", Text = "19" },
                new SelectListItem() {Value = "20", Text = "20" },
                new SelectListItem() {Value = "21", Text = "21" },
                new SelectListItem() {Value = "22", Text = "22" },
                new SelectListItem() {Value = "23", Text = "23" },
                new SelectListItem() {Value = "24", Text = "24" },
                new SelectListItem() {Value = "25", Text = "25" },
                new SelectListItem() {Value = "26", Text = "26" },
                new SelectListItem() {Value = "27", Text = "27" },
                new SelectListItem() {Value = "28", Text = "28" },
                new SelectListItem() {Value = "29", Text = "29" },
                new SelectListItem() {Value = "30", Text = "30" },
                new SelectListItem() {Value = "31", Text = "31" },
                new SelectListItem() {Value = "32", Text = "32" },
                new SelectListItem() {Value = "33", Text = "33" },
                new SelectListItem() {Value = "34", Text = "34" },
                new SelectListItem() {Value = "35", Text = "35" },
                new SelectListItem() {Value = "36", Text = "36" },
                new SelectListItem() {Value = "37", Text = "37" },
                new SelectListItem() {Value = "38", Text = "38" },
                new SelectListItem() {Value = "39", Text = "39" },
                new SelectListItem() {Value = "40", Text = "40" },
                new SelectListItem() {Value = "41", Text = "41" },
                new SelectListItem() {Value = "42", Text = "42" },
                new SelectListItem() {Value = "43", Text = "43" },
                new SelectListItem() {Value = "44", Text = "44" },
                new SelectListItem() {Value = "45", Text = "45" },
                new SelectListItem() {Value = "46", Text = "46" },
                new SelectListItem() {Value = "47", Text = "47" },
                new SelectListItem() {Value = "48", Text = "48" },
                new SelectListItem() {Value = "49", Text = "49" },
                new SelectListItem() {Value = "50", Text = "50" },
                new SelectListItem() {Value = "51", Text = "51" },
                new SelectListItem() {Value = "52", Text = "52" },
                new SelectListItem() {Value = "53", Text = "53" },
                new SelectListItem() {Value = "54", Text = "54" },
                new SelectListItem() {Value = "55", Text = "55" },
                new SelectListItem() {Value = "56", Text = "56" },
                new SelectListItem() {Value = "57", Text = "57" },
                new SelectListItem() {Value = "58", Text = "58" },
                new SelectListItem() {Value = "59", Text = "59" },
                new SelectListItem() {Value = "60", Text = "60" },
                new SelectListItem() {Value = "61", Text = "61" },
                new SelectListItem() {Value = "62", Text = "62" },
                new SelectListItem() {Value = "63", Text = "63" },
                new SelectListItem() {Value = "64", Text = "64" },
                new SelectListItem() {Value = "65", Text = "65" },
                new SelectListItem() {Value = "66", Text = "66" },
                new SelectListItem() {Value = "67", Text = "67" },
                new SelectListItem() {Value = "68", Text = "68" },
                new SelectListItem() {Value = "69", Text = "69" },
                new SelectListItem() {Value = "70", Text = "70" },
                new SelectListItem() {Value = "71", Text = "71" },
                new SelectListItem() {Value = "72", Text = "72" },
                new SelectListItem() {Value = "73", Text = "73" },
                new SelectListItem() {Value = "74", Text = "74" },
                new SelectListItem() {Value = "75", Text = "75" },
                new SelectListItem() {Value = "76", Text = "76" },
                new SelectListItem() {Value = "77", Text = "77" },
                new SelectListItem() {Value = "78", Text = "78" },
                new SelectListItem() {Value = "79", Text = "79" },
                new SelectListItem() {Value = "80", Text = "80" },
                new SelectListItem() {Value = "81", Text = "81" },
                new SelectListItem() {Value = "82", Text = "82" },
                new SelectListItem() {Value = "83", Text = "83" },
                new SelectListItem() {Value = "84", Text = "84" },
                new SelectListItem() {Value = "85", Text = "85" },
                new SelectListItem() {Value = "86", Text = "86" },
                new SelectListItem() {Value = "87", Text = "87" },
                new SelectListItem() {Value = "88", Text = "88" },
                new SelectListItem() {Value = "89", Text = "89" },
                new SelectListItem() {Value = "90", Text = "90" },
                new SelectListItem() {Value = "91", Text = "91" },
                new SelectListItem() {Value = "92", Text = "92" },
                new SelectListItem() {Value = "93", Text = "93" },
                new SelectListItem() {Value = "94", Text = "94" },
                new SelectListItem() {Value = "95", Text = "95" },
                new SelectListItem() {Value = "96", Text = "96" },
                new SelectListItem() {Value = "97", Text = "97" },
                new SelectListItem() {Value = "98", Text = "98" },
                new SelectListItem() {Value = "99", Text = "99" },
                new SelectListItem() {Value = "100", Text = "100" },
                new SelectListItem() {Value = "101", Text = "101" },
                new SelectListItem() {Value = "102", Text = "102" },
                new SelectListItem() {Value = "103", Text = "103" },
                new SelectListItem() {Value = "104", Text = "104" },
                new SelectListItem() {Value = "105", Text = "105" },
                new SelectListItem() {Value = "106", Text = "106" },
                new SelectListItem() {Value = "107", Text = "107" },
                new SelectListItem() {Value = "108", Text = "108" },
                new SelectListItem() {Value = "109", Text = "109" },
                new SelectListItem() {Value = "110", Text = "110" },
                new SelectListItem() {Value = "111", Text = "111" },
                new SelectListItem() {Value = "112", Text = "112" },
                new SelectListItem() {Value = "113", Text = "113" },
                new SelectListItem() {Value = "114", Text = "114" },
                new SelectListItem() {Value = "115", Text = "115" },
                new SelectListItem() {Value = "116", Text = "116" },
                new SelectListItem() {Value = "117", Text = "117" },
                new SelectListItem() {Value = "118", Text = "118" },
                new SelectListItem() {Value = "119", Text = "119" },
                new SelectListItem() {Value = "120", Text = "120" }
            };
            return ageList;
        }

            private List<SelectListItem> getGendersList()
        {
            var genderList = new List<SelectListItem>()
            {
                new SelectListItem() {Value = "Male", Text = "Male" },
                new SelectListItem() {Value = "Female", Text = "Female" },
                new SelectListItem() {Value = "Other", Text = "Other" }
            };
            return genderList;
        }
        private List<SelectListItem> getEducationsList()
        {
            var educationList = new List<SelectListItem>()
            {
                new SelectListItem(){Value= "Secondary Education", Text="Secondary Education"},
                new SelectListItem() {Value = "Bachelors Degree", Text = "Bachelors Degree" },
                new SelectListItem() {Value = "Masters Degree", Text = "Masters Degree" },
                new SelectListItem() {Value = "PhD", Text = "PhD" },
                new SelectListItem() {Value = "Other", Text = "Other" }

            };
            return educationList;
        }
        private List<SelectListItem> getCountriesList()
        {
            var CountriesList = new List<SelectListItem>()
            {
                new SelectListItem() {Value = "United States of America (USA)",     Text = "United States of America (USA)" },
                new SelectListItem() {Value = "India",                              Text = "India" },
                new SelectListItem() {Value = "Afghanistan",                        Text = "Afghanistan" },
                new SelectListItem() {Value = "Albania",                            Text = "Albania" },
                new SelectListItem() {Value = "Algeria",                            Text = "Algeria" },
                new SelectListItem() {Value = "Andorra",                            Text = "Andorra" },
                new SelectListItem() {Value = "Angola",                             Text = "Angola" },
                new SelectListItem() {Value = "Antigua and Barbuda",                Text = "Antigua and Barbuda" },
                new SelectListItem() {Value = "Argentina",                          Text = "Argentina" },
                new SelectListItem() {Value = "Armenia",                            Text = "Armenia" },
                new SelectListItem() {Value = "Australia",                          Text = "Australia" },
                new SelectListItem() {Value = "Austria",                            Text = "Austria" },
                new SelectListItem() {Value = "Azerbaijan",                         Text = "Azerbaijan" },
                new SelectListItem() {Value = "Bahamas",                            Text = "Bahamas" },
                new SelectListItem() {Value = "Bahrain",                            Text = "Bahrain" },
                new SelectListItem() {Value = "Bangladesh",                         Text = "Bangladesh" },
                new SelectListItem() {Value = "Barbados",                           Text = "Barbados" },
                new SelectListItem() {Value = "Belarus",                            Text = "Belarus" },
                new SelectListItem() {Value = "Belgium",                            Text = "Belgium" },
                new SelectListItem() {Value = "Belize",                             Text = "Belize" },
                new SelectListItem() {Value = "Benin",                              Text = "Benin" },
                new SelectListItem() {Value = "Bhutan",                             Text = "Bhutan" },
                new SelectListItem() {Value = "Bolivia",                            Text = "Bolivia" },
                new SelectListItem() {Value = "Bosnia and Herzegovina",             Text = "Bosnia and Herzegovina" },
                new SelectListItem() {Value = "Botswana",                           Text = "Botswana" },
                new SelectListItem() {Value = "Brazil",                             Text = "Brazil" },
                new SelectListItem() {Value = "Brunei",                             Text = "Brunei" },
                new SelectListItem() {Value = "Bulgaria",                           Text = "Bulgaria" },
                new SelectListItem() {Value = "Burkina Faso",                       Text = "Burkina Faso" },
                new SelectListItem() {Value = "Burundi",                            Text = "Burundi" },
                new SelectListItem() {Value = "Cabo Verde",                         Text = "Cabo Verde" },
                new SelectListItem() {Value = "Cambodia",                           Text = "Cambodia" },
                new SelectListItem() {Value = "Cameroon",                           Text = "Cameroon" },
                new SelectListItem() {Value = "Canada",                             Text = "Canada" },
                new SelectListItem() {Value = "Central African Republic",           Text = "Central African Republic" },
                new SelectListItem() {Value = "Chad",                               Text = "Chad" },
                new SelectListItem() {Value = "Chile",                              Text = "Chile" },
                new SelectListItem() {Value = "China",                              Text = "China" },
                new SelectListItem() {Value = "Colombia",                           Text = "Colombia" },
                new SelectListItem() {Value = "Comoros",                            Text = "Comoros" },
                new SelectListItem() {Value = "Congo, Republic of the",             Text = "Congo, Republic of the" },
                new SelectListItem() {Value = "Congo, Democratic Republic of the",  Text = "Congo, Democratic Republic of the" },
                new SelectListItem() {Value = "Costa Rica",                         Text = "Costa Rica" },
                new SelectListItem() {Value = "Cote d'Ivoire",                      Text = "Cote d'Ivoire" },
                new SelectListItem() {Value = "Croatia",                            Text = "Croatia" },
                new SelectListItem() {Value = "Cuba",                               Text = "Cuba" },
                new SelectListItem() {Value = "Cyprus",                             Text = "Cyprus" },
                new SelectListItem() {Value = "Czech Republic",                     Text = "Czech Republic" },
                new SelectListItem() {Value = "Denmark",                            Text = "Denmark" },
                new SelectListItem() {Value = "Djibouti",                           Text = "Djibouti" },
                new SelectListItem() {Value = "Dominica",                           Text = "Dominica" },
                new SelectListItem() {Value = "Dominican Republic",                 Text = "Dominican Republic" },
                new SelectListItem() {Value = "Ecuador",                            Text = "Ecuador" },
                new SelectListItem() {Value = "Egypt",                              Text = "Egypt" },
                new SelectListItem() {Value = "El Salvador",                        Text = "El Salvador" },
                new SelectListItem() {Value = "Equatorial Guinea",                  Text = "Equatorial Guinea" },
                new SelectListItem() {Value = "Eritrea",                            Text = "Eritrea" },
                new SelectListItem() {Value = "Estonia",                            Text = "Estonia" },
                new SelectListItem() {Value = "Ethiopia",                           Text = "Ethiopia" },
                new SelectListItem() {Value = "Fiji",                               Text = "Fiji" },
                new SelectListItem() {Value = "Finland",                            Text = "Finland" },
                new SelectListItem() {Value = "France",                             Text = "France" },
                new SelectListItem() {Value = "Gabon",                              Text = "Gabon" },
                new SelectListItem() {Value = "Gambia",                             Text = "Gambia" },
                new SelectListItem() {Value = "Georgia",                            Text = "Georgia" },
                new SelectListItem() {Value = "Germany",                            Text = "Germany" },
                new SelectListItem() {Value = "Ghana",                              Text = "Ghana" },
                new SelectListItem() {Value = "Greece",                             Text = "Greece" },
                new SelectListItem() {Value = "Grenada",                            Text = "Grenada" },
                new SelectListItem() {Value = "Guatemala",                          Text = "Guatemala" },
                new SelectListItem() {Value = "Guinea",                             Text = "Guinea" },
                new SelectListItem() {Value = "Guinea-Bissau",                      Text = "Guinea-Bissau" },
                new SelectListItem() {Value = "Guyana",                             Text = "Guyana" },
                new SelectListItem() {Value = "Haiti",                              Text = "Haiti" },
                new SelectListItem() {Value = "Honduras",                           Text = "Honduras" },
                new SelectListItem() {Value = "Hungary",                            Text = "Hungary" },
                new SelectListItem() {Value = "Iceland",                            Text = "Iceland" },
                new SelectListItem() {Value = "Indonesia",                          Text = "Indonesia" },
                new SelectListItem() {Value = "Iran",                               Text = "Iran" },
                new SelectListItem() {Value = "Iraq",                               Text = "Iraq" },
                new SelectListItem() {Value = "Ireland",                            Text = "Ireland" },
                new SelectListItem() {Value = "Israel",                             Text = "Israel" },
                new SelectListItem() {Value = "Italy",                              Text = "Italy" },
                new SelectListItem() {Value = "Jamaica",                            Text = "Jamaica" },
                new SelectListItem() {Value = "Japan",                              Text = "Japan" },
                new SelectListItem() {Value = "Jordan",                             Text = "Jordan" },
                new SelectListItem() {Value = "Kazakhstan",                         Text = "Kazakhstan" },
                new SelectListItem() {Value = "Kenya",                              Text = "Kenya" },
                new SelectListItem() {Value = "Kiribati",                           Text = "Kiribati" },
                new SelectListItem() {Value = "Kosovo",                             Text = "Kosovo" },
                new SelectListItem() {Value = "Kuwait",                             Text = "Kuwait" },
                new SelectListItem() {Value = "Kyrgyzstan",                         Text = "Kyrgyzstan" },
                new SelectListItem() {Value = "Laos",                               Text = "Laos" },
                new SelectListItem() {Value = "Latvia",                             Text = "Latvia" },
                new SelectListItem() {Value = "Lebanon",                            Text = "Lebanon" },
                new SelectListItem() {Value = "Lesotho",                            Text = "Lesotho" },
                new SelectListItem() {Value = "Liberia",                            Text = "Liberia" },
                new SelectListItem() {Value = "Libya",                              Text = "Libya" },
                new SelectListItem() {Value = "Liechtenstein",                      Text = "Liechtenstein" },
                new SelectListItem() {Value = "Lithuania",                          Text = "Lithuania" },
                new SelectListItem() {Value = "Luxembourg",                         Text = "Luxembourg" },
                new SelectListItem() {Value = "Macedonia",                          Text = "Macedonia" },
                new SelectListItem() {Value = "Madagascar",                         Text = "Madagascar" },
                new SelectListItem() {Value = "Malawi",                             Text = "Malawi" },
                new SelectListItem() {Value = "Malaysia",                           Text = "Malaysia" },
                new SelectListItem() {Value = "Maldives",                           Text = "Maldives" },
                new SelectListItem() {Value = "Mali",                               Text = "Mali" },
                new SelectListItem() {Value = "Malta",                              Text = "Malta" },
                new SelectListItem() {Value = "Marshall Islands",                   Text = "Marshall Islands" },
                new SelectListItem() {Value = "Mauritania",                         Text = "Mauritania" },
                new SelectListItem() {Value = "Mauritius",                          Text = "Mauritius" },
                new SelectListItem() {Value = "Mexico",                             Text = "Mexico" },
                new SelectListItem() {Value = "Micronesia",                         Text = "Micronesia" },
                new SelectListItem() {Value = "Moldova",                            Text = "Moldova" },
                new SelectListItem() {Value = "Monaco",                             Text = "Monaco" },
                new SelectListItem() {Value = "Mongolia",                           Text = "Mongolia" },
                new SelectListItem() {Value = "Montenegro",                         Text = "Montenegro" },
                new SelectListItem() {Value = "Morocco",                            Text = "Morocco" },
                new SelectListItem() {Value = "Mozambique",                         Text = "Mozambique" },
                new SelectListItem() {Value = "Myanmar (Burma)",                    Text = "Myanmar (Burma)" },
                new SelectListItem() {Value = "Namibia",                            Text = "Namibia" },
                new SelectListItem() {Value = "Nauru",                              Text = "Nauru" },
                new SelectListItem() {Value = "Nepal",                              Text = "Nepal" },
                new SelectListItem() {Value = "Netherlands",                        Text = "Netherlands" },
                new SelectListItem() {Value = "New Zealand",                        Text = "New Zealand" },
                new SelectListItem() {Value = "Nicaragua",                          Text = "Nicaragua" },
                new SelectListItem() {Value = "Niger",                              Text = "Niger" },
                new SelectListItem() {Value = "Nigeria",                            Text = "Nigeria" },
                new SelectListItem() {Value = "North Korea",                        Text = "North Korea" },
                new SelectListItem() {Value = "Norway",                             Text = "Norway" },
                new SelectListItem() {Value = "Oman",                               Text = "Oman" },
                new SelectListItem() {Value = "Pakistan",                           Text = "Pakistan" },
                new SelectListItem() {Value = "Palau",                              Text = "Palau" },
                new SelectListItem() {Value = "Palestine",                          Text = "Palestine" },
                new SelectListItem() {Value = "Panama",                             Text = "Panama" },
                new SelectListItem() {Value = "Papua New Guinea",                   Text = "Papua New Guinea" },
                new SelectListItem() {Value = "Paraguay",                           Text = "Paraguay" },
                new SelectListItem() {Value = "Peru",                               Text = "Peru" },
                new SelectListItem() {Value = "Philippines",                        Text = "Philippines" },
                new SelectListItem() {Value = "Poland",                             Text = "Poland" },
                new SelectListItem() {Value = "Portugal",                           Text = "Portugal" },
                new SelectListItem() {Value = "Qatar",                              Text = "Qatar" },
                new SelectListItem() {Value = "Romania",                            Text = "Romania" },
                new SelectListItem() {Value = "Russia",                             Text = "Russia" },
                new SelectListItem() {Value = "Rwanda",                             Text = "Rwanda" },
                new SelectListItem() {Value = "St. Kitts and Nevis",                Text = "St. Kitts and Nevis" },
                new SelectListItem() {Value = "St. Lucia",                          Text = "St. Lucia" },
                new SelectListItem() {Value = "St. Vincent and The Grenadines",     Text = "St. Vincent and The Grenadines" },
                new SelectListItem() {Value = "Samoa",                              Text = "Samoa" },
                new SelectListItem() {Value = "San Marino",                         Text = "San Marino" },
                new SelectListItem() {Value = "Sao Tome and Principe",              Text = "Sao Tome and Principe" },
                new SelectListItem() {Value = "Saudi Arabia",                       Text = "Saudi Arabia" },
                new SelectListItem() {Value = "Senegal",                            Text = "Senegal" },
                new SelectListItem() {Value = "Serbia",                             Text = "Serbia" },
                new SelectListItem() {Value = "Seychelles",                         Text = "Seychelles" },
                new SelectListItem() {Value = "Sierra Leone",                       Text = "Sierra Leone" },
                new SelectListItem() {Value = "Singapore",                          Text = "Singapore" },
                new SelectListItem() {Value = "Slovakia",                           Text = "Slovakia" },
                new SelectListItem() {Value = "Slovenia",                           Text = "Slovenia" },
                new SelectListItem() {Value = "Solomon Islands",                    Text = "Solomon Islands" },
                new SelectListItem() {Value = "Somalia",                            Text = "Somalia" },
                new SelectListItem() {Value = "South Africa",                       Text = "South Africa" },
                new SelectListItem() {Value = "South Korea",                        Text = "South Korea" },
                new SelectListItem() {Value = "South Sudan",                        Text = "South Sudan" },
                new SelectListItem() {Value = "Spain",                              Text = "Spain" },
                new SelectListItem() {Value = "Sri Lanka",                          Text = "Sri Lanka" },
                new SelectListItem() {Value = "Sudan",                              Text = "Sudan" },
                new SelectListItem() {Value = "Suriname",                           Text = "Suriname" },
                new SelectListItem() {Value = "Swaziland",                          Text = "Swaziland" },
                new SelectListItem() {Value = "Sweden",                             Text = "Sweden" },
                new SelectListItem() {Value = "Switzerland",                        Text = "Switzerland" },
                new SelectListItem() {Value = "Syria",                              Text = "Syria" },
                new SelectListItem() {Value = "Taiwan",                             Text = "Taiwan" },
                new SelectListItem() {Value = "Tajikistan",                         Text = "Tajikistan" },
                new SelectListItem() {Value = "Tanzania",                           Text = "Tanzania" },
                new SelectListItem() {Value = "Thailand",                           Text = "Thailand" },
                new SelectListItem() {Value = "Timor-Leste",                        Text = "Timor-Leste" },
                new SelectListItem() {Value = "Togo",                               Text = "Togo" },
                new SelectListItem() {Value = "Tonga",                              Text = "Tonga" },
                new SelectListItem() {Value = "Trinidad and Tobago",                Text = "Trinidad and Tobago" },
                new SelectListItem() {Value = "Tunisia",                            Text = "Tunisia" },
                new SelectListItem() {Value = "Turkey",                             Text = "Turkey" },
                new SelectListItem() {Value = "Turkmenistan",                       Text = "Turkmenistan" },
                new SelectListItem() {Value = "Tuvalu",                             Text = "Tuvalu" },
                new SelectListItem() {Value = "Uganda",                             Text = "Uganda" },
                new SelectListItem() {Value = "Ukraine",                            Text = "Ukraine" },
                new SelectListItem() {Value = "United Arab Emirates",               Text = "United Arab Emirates" },
                new SelectListItem() {Value = "United Kingdom (UK)",                Text = "United Kingdom (UK)" },
                new SelectListItem() {Value = "Uruguay",                            Text = "Uruguay" },
                new SelectListItem() {Value = "Uzbekistan",                         Text = "Uzbekistan" },
                new SelectListItem() {Value = "Vanuatu",                            Text = "Vanuatu" },
                new SelectListItem() {Value = "Vatican City (Holy See)",            Text = "Vatican City (Holy See)" },
                new SelectListItem() {Value = "Venezuela",                          Text = "Venezuela" },
                new SelectListItem() {Value = "Vietnam",                            Text = "Vietnam" },
                new SelectListItem() {Value = "Yemen",                              Text = "Yemen" },
                new SelectListItem() {Value = "Zambia",                             Text = "Zambia" },
                new SelectListItem() {Value = "Zimbabwe",                           Text = "Zimbabwe" }

            };
            return CountriesList;
        }
    }
}