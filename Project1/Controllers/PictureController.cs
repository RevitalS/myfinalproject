﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project1.Utility;
using Project1.Models;
using Project1.Constants;
using System.Data.Entity;

namespace Project1.Controllers
{
    /// <summary>
    /// Manage all the images - save the opening order, and times
    /// </summary>
    [NoCache]
    public class PictureController : Controller
    {
        [HttpGet]
        public ActionResult Start()
        {
            PictureSessionManager.InitClickList();
            return View();
        }

        [HttpGet]
        public ActionResult ManagePic()
        {
            if (TimeSessionManager.GetViewPictureTimer() == null)
            {
                TimeSessionManager.StopMethodTimer();
                TimeSessionManager.StartViewPictureTimer();
                PictureSessionManager.SetFlag(-1);
            }
            if (ExperimentSessionManager.GetState().Equals(ExperimentState.InPicture) && PictureSessionManager.GetFlag() > 0)
            {
                return RedirectToAction("PictureInstructions", "Instructions");
                //return RedirectToAction("Picture", new { id = PictureSessionManager.GetFlag().ToString()});
            }
            //cencel back and set the current state
            if (ExperimentSessionManager.GetState() > ExperimentState.Experiment)
            {
                return RedirectToAction("PictureInstructions", "Instructions");
            }
            ExperimentSessionManager.SetState(ExperimentState.Experiment);

            var cp = new ChosenPictures();
            cp.Method = (ExperimentMethods)PictureSessionManager.GetMethod();
           // ViewBag.PictureNum = GameConstants.NumberOfPictures;
            cp.NumberOfPic = GameConstants.NumberOfPictures;
            return View(cp);
        }

        [HttpPost]
        public ActionResult ManagePic(PicturesUsers model)
        {
            if (model.ChosenImage != null)
            {
                if (AMTSessionManager.GetWorkerId() != "friend1")
                {
                    using (var db = new MainDbContext())
                    {
                        var workerId = AMTSessionManager.GetWorkerId();
                        var usr = db.PicturesUsers.Where(e => e.WorkerId.Equals(workerId)).FirstOrDefault();
                        usr.ChosenImage = model.ChosenImage;
                        usr.NumberOfLookedImages = PictureSessionManager.GetClickList().Count();
                        if (PictureSessionManager.GetClickList().Count().Equals(GameConstants.NumberOfPictures))
                        {
                            usr.CompleteViewImages = true;
                        }
                        else
                        {
                            usr.CompleteViewImages = false;
                        }

                        db.Entry(usr).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                TimeSessionManager.StopViewPictureTimer();
                if (PictureSessionManager.GetMethod().Equals(ExperimentMethods.SignAtEnd))
                {
                    return RedirectToAction("Sign", "Methods"); 
                }
                return RedirectToAction("LastPage", "Summary");
            }
            else
            {
                var cp = new ChosenPictures();
                cp.NumberOfPic = 30;
                cp.DidChoose = false;
                return View(cp);
            }
        }

        [HttpGet]
        public ActionResult OpenPictureTime(string val)
        {
            PictureSessionManager.SetOpenPicureTime(val);
            return RedirectToAction("Picture", new { id = val.ToString() });
               }

        [HttpGet]
        public ActionResult CountClick(string val)
        {
            PictureSessionManager.SetClickList(val);
            ExperimentSessionManager.SetState(ExperimentState.Experiment);
            PictureSessionManager.SetFlag(-1);
            return RedirectToAction("ManagePic");
        }
        [HttpGet]
        public ActionResult Picture(string id)
        {
            if (TimeSessionManager.GetStartTime() == null)
            {
                return RedirectToAction("ManagePic");
            }
            if (id.Equals("InvalidAccess"))
            {
                return RedirectToAction("InvalidAccess");
            }
            ExperimentSessionManager.SetState(ExperimentState.InPicture);
            //var pic = GetPictureByOrderAndIndex(int.Parse(id));
            var pic = "";
            if (id.Equals("11") && PictureSessionManager.GetMethod().Equals(ExperimentMethods.FakePage))
            {
                pic = "fake.jpg";
            }
            else
            {
                if (int.Parse(id) < 10)
                {
                    pic = "picture" + id + ".jpeg";
                }
                else
                {
                    pic = "picture" + id + ".jpg";
                }
            }
            ViewBag.Massage = id;
            ViewBag.Src = Url.Content("~/pic/" + pic);
            ViewBag.Method = (ExperimentMethods)PictureSessionManager.GetMethod();
            PictureSessionManager.SetFlag(int.Parse(id));
            return View();
        }

        public ActionResult InvalidAccess()
        {
            return View();
        }


        private List<SelectListItem> getNumList()
        {
            var numList = new List<SelectListItem>()
            {
                new SelectListItem() {Value = "1", Text = "1" },
                new SelectListItem() {Value = "2", Text = "2" },
                new SelectListItem() {Value = "3", Text = "3" },
                new SelectListItem() {Value = "4", Text = "4" },
                new SelectListItem() {Value = "5", Text = "5" },
                new SelectListItem() {Value = "6", Text = "6" },
                new SelectListItem() {Value = "7", Text = "7" },
                new SelectListItem() {Value = "8", Text = "8" },
                new SelectListItem() {Value = "9", Text = "9" },
                new SelectListItem() {Value = "10", Text = "10" },
                new SelectListItem() {Value = "11", Text = "11" },
                new SelectListItem() {Value = "12", Text = "12" },
                new SelectListItem() {Value = "13", Text = "13" },
                new SelectListItem() {Value = "14", Text = "14" },
                new SelectListItem() {Value = "15", Text = "15" },
                new SelectListItem() {Value = "16", Text = "16" },
                new SelectListItem() {Value = "17", Text = "17" },
                new SelectListItem() {Value = "18", Text = "18" },
                new SelectListItem() {Value = "19", Text = "19" },
                new SelectListItem() {Value = "20", Text = "20" },
                new SelectListItem() {Value = "21", Text = "21" },
                new SelectListItem() {Value = "22", Text = "22" },
                new SelectListItem() {Value = "23", Text = "23" },
                new SelectListItem() {Value = "24", Text = "24" },
                new SelectListItem() {Value = "25", Text = "25" },
                new SelectListItem() {Value = "26", Text = "26" },
                new SelectListItem() {Value = "27", Text = "27" },
                new SelectListItem() {Value = "28", Text = "28" },
                new SelectListItem() {Value = "29", Text = "29" },
                new SelectListItem() {Value = "30", Text = "30" }
                            };
            return numList;
        }

    }
}