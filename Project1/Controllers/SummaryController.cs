﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project1.Models;
using System.Data;
using System.Data.Entity;
using Project1.Utility;
using Project1.Constants;
using Project1.ViewModel;

namespace Project1.Controllers
{
    /// <summary>
    /// The end of the experiment and save the times of user.
    /// </summary>
    public class SummaryController : Controller
    {
        [HttpGet]
        public ActionResult LastPage()
        {
            TimeSessionManager.StopGameTimer();
            // ViewBag.Pay = GameConstants.Payment.ToString();


            if (AMTSessionManager.GetWorkerId() != "friend1")
            {
                using (var db = new MainDbContext())
                {
                    var workerId = AMTSessionManager.GetWorkerId();
                    var usr = db.PicturesUsers.Where(e => e.WorkerId.Equals(workerId)).FirstOrDefault();
                    //add times to table
                    usr.WorkerId = AMTSessionManager.GetWorkerId();
                    usr.InstructionsTime = TimeSessionManager.GetInstructionsTimer();
                    usr.TrainingTime = TimeSessionManager.GetTrainingTimer();
                    usr.QuizTime = TimeSessionManager.GetQuizTimer();
                    usr.UserProfileTime = TimeSessionManager.GetUserProfileTimer();
                    usr.MethodTime = TimeSessionManager.GetMethodTimer();
                    usr.ViewImagesTime = TimeSessionManager.GetViewPictureTimer();
                    usr.ExperimentTime = TimeSessionManager.GetGameTimer();


                    //open picture order and time
                    var x = PictureSessionManager.GetClickListString();
                    usr.StringOpenPicturesOrder = PictureSessionManager.GetClickListString();
                    usr.StringOpenPicturesTime = PictureSessionManager.GetOpenImagesTimeString();
                    usr.StringTimeInPicture = PictureSessionManager.GetTimeInImageString();
                    db.Entry(usr).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

            //set the current state - complete
            ExperimentSessionManager.SetState(ExperimentState.Completed);
            var model = new AMTModel();
            model.AssignmentId = AMTSessionManager.GetAssignmentId();
            model.HitId = AMTSessionManager.GetHitId();
            model.WorkerId = AMTSessionManager.GetWorkerId();
            model.TurkSubmitToUrl = AMTSessionManager.GetTurkSubmitTo() +"/mturk/externalSubmit";
            model.Payment = GameConstants.Payment.ToString();

            return View(model);
        }

    }
}