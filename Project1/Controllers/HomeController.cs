﻿using Project1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project1.Utility;
using Project1.Constants;
using Project1.Parsers;
using Project1.ViewModel;

namespace Project1.Controllers
{
    /// <summary>
    /// Getting all the user date from AMT, and choosing Method.
    /// </summary>
    [NoCache]
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            string assignmentId = Request.QueryString["assignmentId"];
            string workerId = Request.QueryString["workerId"];
            string hitId = Request.QueryString["hitId"];
            string turkSubmitTo = assignmentId;
            var AMTmodel = new AMTModel();
            TimeSessionManager.StartGameTimer();
            ExperimentSessionManager.SetState(ExperimentState.Instructions);
            //AMTModel amtModel = new AMTModel();
            if (AMTSessionManager.GetWorkerId() != null)
            {
                Session.Abandon();
                return RedirectToAction("Index", "Home", new { assignmentId = assignmentId, hitId = hitId, turkSubmitTo = turkSubmitTo, workerId = workerId });
            }

             using (var db = new MainDbContext())
             {
                 //check if user alrady priticipate
                 var user = db.PicturesUsers.Where(e => e.WorkerId.Equals(workerId)).FirstOrDefault();
                 if (user != null)
                     return RedirectToAction("UserExists", "Error");
                 //check if still have games to play
                 var methodsList = db.Methods.Where(e => e.Counter.Equals(0)).ToList();
                 if (methodsList.Equals(GameConstants.NumberOfMethods))
                 {
                     return RedirectToAction("OutOfData", "Error");
                 }
                     //didn't choose the HIT
                     if (((assignmentId != null) && (assignmentId.Equals("ASSIGNMENT_ID_NOT_AVAILABLE"))) || (assignmentId == null))

                 {
                         //friend assignment
                         if (assignmentId == null)
                         {
                        Random rnd = new Random();
                        workerId = "A"+ rnd.Next(1000, 9999).ToString();
                             //workerId = "friend1";
                            turkSubmitTo = "turtAss";
                             hitId = "hit id friend";
                             assignmentId = "aaa"; //DEBUG FOR MYSELF
                         PictureSessionManager.SetMethod(ExperimentMethods.None);
                         }
                     AMTSessionManager.SetAssignmentId(assignmentId);
                     // var entryList = GetPossibleEntryList();
                   //  var methodsList = db.Methods.Where(e =>e.Counter.Equals(0)).ToList();
                    // if (methodsList.Equals(GameConstants.NumberOfMethods)){ 
                      //   return RedirectToAction("OutOfData", "Error");
                         ///////////////////////////////////////////////////////////////////////
                         ////////////////////////////////////////////////////////////////////////
                     // ***************** let the see first page, can't press continue.*************
                     AMTmodel.IsAccepted = false;
                 }
                 else
                 {
                     //set method
                     //var method = db.Methods.Create();
                     AMTmodel.IsAccepted = true;
                     Random rnd = new Random();
                     int methodNum = rnd.Next(1, GameConstants.NumberOfMethods + 1);
                     var o = db.Methods.Where(e => e.Id.Equals(((ExperimentMethods)methodNum).ToString())).FirstOrDefault().Counter;
                     while (o.Equals(0))
                     {
                         methodNum = rnd.Next(1, GameConstants.NumberOfMethods + 1);
                         o = db.Methods.Where(e => e.Id.Equals(((ExperimentMethods)methodNum).ToString())).FirstOrDefault().Counter;

                     }
                     o--;
                     db.Methods.Where(e => e.Id.Equals(((ExperimentMethods)methodNum).ToString())).FirstOrDefault().Counter = o;
                     db.SaveChanges();
                     PictureSessionManager.SetMethod((ExperimentMethods)methodNum);

                     }

                 }
            //workerId = "friend1";
            /*turkSubmitTo = "turtAss";
            hitId = "hit id friend";
            assignmentId = "aaa"; //DEBUG FOR MYSELF
            Random rnd = new Random();
            int chooseRndNum = rnd.Next(1000, 9999);
            workerId = chooseRndNum.ToString();
            AMTSessionManager.SetWorkerId(chooseRndNum.ToString());*/
            AMTSessionManager.SetWorkerId(workerId);
            AMTSessionManager.SetTurkSubmitTo(turkSubmitTo);
            AMTSessionManager.SetHitId(hitId);
            AMTSessionManager.SetAssignmentId(assignmentId);
            PictureSessionManager.SetMethod(ExperimentMethods.None);
            PictureSessionManager.InitClickList();
            PictureSessionManager.InitPictureTimeList();
            return RedirectToAction("PictureInstructions", "Instructions");

        }



      /*  [HttpGet]
        public ActionResult Index()
        {
            var x = OrdersParser.GetOrders();
            //Init counters for order and methods
              using (var db = new MainDbContext())
            {

               /*    for (int i = 1; i <= GameConstants.NumberOfOrders; i++)
                   {
                    var b = db.Methods.Create();
                    b.Counter = 10;
                    b.Id =(ExperimentMethods.MoralPicture).ToString();
                       db.Methods.Add(b);
                       db.SaveChanges();
                   }
                */  
               // var c = db.Orders.Where(e => e.Name.Equals(2)).FirstOrDefault().Counter = 5;
                //db.Orders.Remove(c);
               // c.Counter = 5;

                //db.SaveChanges();
                //db.Orders.Add(c);
                //db.SaveChanges();
                
          /*  }

            TimeSessionManager.StartGameTimer();
            ExperimentSessionManager.SetState(ExperimentState.Instructions);
            Random rnd = new Random();
            int chooseRndNum = rnd.Next(1000, 9999);
            AMTSessionManager.SetWorkerId(chooseRndNum.ToString());
            AMTSessionManager.SetAssignmentId(chooseRndNum.ToString());
            AMTSessionManager.SetHitId(chooseRndNum.ToString());

            var w = PictureSessionManager.GetOrder();
            PictureSessionManager.InitClickList();
            PictureSessionManager.InitPictureTimeList();
            using (var db = new MainDbContext())
            {

                //set order

                //set method
                //var method = db.Methods.Create();
                int methodNum = rnd.Next(1, GameConstants.NumberOfMethods + 1);
                var o = db.Methods.Where(e => e.Id.Equals(((ExperimentMethods) methodNum).ToString())).FirstOrDefault().Counter;
                while (o.Equals(0))
                {
                    methodNum = rnd.Next(1, GameConstants.NumberOfMethods + 1);
                    o = db.Methods.Where(e => e.Id.Equals(((ExperimentMethods) methodNum).ToString())).FirstOrDefault().Counter;

                }
                o--;
                db.Methods.Where(e => e.Id.Equals(((ExperimentMethods) methodNum).ToString())).FirstOrDefault().Counter = o;
                db.SaveChanges();
                PictureSessionManager.SetMethod((ExperimentMethods)methodNum);

               /* var picUser = db.PicturesUsers.Create();
                picUser.WorkerId = AMTSessionManager.GetWorkerId();
                picUser.Age = "100";
                picUser.Gender = "Fmale";
                picUser.Country = "Israel";
                picUser.Education = "PH.D";
                picUser.StartTime = TimeSessionManager.GetStartTime();
                picUser.Order = PictureSessionManager.GetOrder().ToString();
                db.PicturesUsers.Add(picUser);
                db.SaveChanges();
                */

          /*  }
            //return View();
            return RedirectToAction("PictureInstructions", "Instructions");
        }

        private bool GetDataFromAMT()
        {
            string mobile = "not_mobile";
            if (Request.Browser.IsMobileDevice)
            {
                mobile = "mobile_user";
            }

            string assignmentId = Request.QueryString["assignmentId"];
            string workerId = Request.QueryString["workerId"];

            //friend assignment
            if (assignmentId == null)
            {
                Session["user_id"] = "friend1";
                Session["turkAss"] = "turkAss";
                Session["hitId"] = "hit id friend";
                //assignmentId = "aaa"; //DEBUG FOR MYSELF
            }

            //from AMT but did not took the assigment (didn't click accept)
            // check whether he has already participated (before 'accept' everyone has userId. After accepting the HIT they get the assignmentId).
            else if (assignmentId.Equals("ASSIGNMENT_ID_NOT_AVAILABLE"))
            {
                using (var db = new MainDbContext())
                {
                    var usr = db.PicturesUsers.Where(e => e.WorkerId.Equals(workerId)).FirstOrDefault();
                    if (usr == null)
                    {
                        var x = 0;
                    }
                }
            }

            //from AMT and accepted the assigment - continue to experiment 
            //if (assignmentId == "aaa") //DEBUG FOR MYSELF
            else
            {
                AMTSessionManager.SetWorkerId(workerId);
                AMTSessionManager.SetHitId(Request.QueryString["hitId"]);
                AMTSessionManager.SetAssignmentId(assignmentId);
                //Session["user_id"] = Request.QueryString["workerId"];   //save participant's user ID
                //Session["turkAss"] = Request.QueryString["assignmentId"]; ;  //save participant's assignment ID
                //Session["hitId"] = Request.QueryString["hitId"];
                //Session["BeginTime"] = DateTime.Now.ToString();

                ////DEBUG FOR MYSELF
                //Session["user_id"] = "aab";
                //Session["turkAss"] = "aaa";
                //Session["hitId"] = "xx";
            }
                
            //Session["value"] = -1;
            //return View();
            return false;

            }*/





    }
}