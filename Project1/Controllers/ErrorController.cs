﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project1.Constants;
using Project1.Utility;
namespace Project1.Controllers
{
    /// <summary>
    /// Errors pages
    /// </summary>
    [NoCache]
    public class ErrorController : Controller
    {

        public ActionResult UserExists()
        {
            return View();
        }
        public ActionResult OutOfData()
        {
            return View();
        }
    }
}