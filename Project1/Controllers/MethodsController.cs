﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project1.Models;
using Project1.Utility;
using Project1.Constants;

namespace Project1.Controllers
{
    /// <summary>
    /// Manage all methods
    /// </summary>
    public class MethodsController : Controller
    {

        // GET: Methods
        public ActionResult Methods()
        {
            TimeSessionManager.StartMethodTimer();

            FlagsSessionManager.SetUserFlag(false);

            switch (PictureSessionManager.GetMethod())
            {
                case ExperimentMethods.None:
                case ExperimentMethods.Eyes:
                case ExperimentMethods.SignAtEnd:
                case ExperimentMethods.MoralPicture:
                case ExperimentMethods.FakePage:
                    return RedirectToAction("ManagePic", "Picture");
                case ExperimentMethods.SignByRecord:
                case ExperimentMethods.SignByListen:
                case ExperimentMethods.SignByRead:
                    break;
                case ExperimentMethods.TenCommandments:
                    return RedirectToAction("TenCommandments");
                case ExperimentMethods.RegularWriteParagraph:
                    return RedirectToAction("Paragraph");

                case ExperimentMethods.SignAtBeginning:
                    return RedirectToAction("Sign");
                case ExperimentMethods.MoralParagraph:
                case ExperimentMethods.RegularParagraph:
                    break;
                default:
                    break;
            }

            return RedirectToAction("ManagePic", "Picture");
        }



        [HttpGet]
        public ActionResult Paragraph()
        {
            //PictureSessionManager.SetMethod(ExperimentMethods.HardWriteParagraph);
            var mv = new MethodsValues();
            mv.ShowValue = GetParagraphString();
            return View(mv);
        }

        [HttpPost]
        public ActionResult Paragraph(MethodsValues model)
        {
            using (var db = new MainDbContext())
            {
                var mv = db.MethodsValues.Create();
                mv.WorkerId = AMTSessionManager.GetWorkerId();
                mv.Method = PictureSessionManager.GetMethod().ToString();
                mv.Value = model.Value;
                db.MethodsValues.Add(mv);
                db.SaveChanges();
            }
            return RedirectToAction("ManagePic", "Picture");

        }

        [HttpGet]
        public ActionResult TenCommandments()
        {
            PictureSessionManager.SetMethod(ExperimentMethods.TenCommandments);
            var mv = new MethodsValues();
            mv.ShowValue = GetParagraphString();
            return View(mv);
        }

        [HttpPost]
        public ActionResult TenCommandments(MethodsValues model)
        {
            if (model.Value == null)
            {
                var mv = new MethodsValues();
                mv.ShowValue = GetParagraphString();
                return View(mv);
            }
            using (var db = new MainDbContext())
            {
                var mv = db.MethodsValues.Create();
                mv.WorkerId = AMTSessionManager.GetWorkerId();
                mv.Method = PictureSessionManager.GetMethod().ToString();
                mv.Value = model.Value;
                db.MethodsValues.Add(mv);
                db.SaveChanges();
            }
            return RedirectToAction("ManagePic", "Picture");

        }
        [HttpGet]
        public ActionResult Sign()
        {
            //PictureSessionManager.SetMethod(ExperimentMethods.SignAtBeginning);
            var mv = new MethodsValues();
            mv.ShowValue = GetParagraphString();
            return View(mv);
        }

        [HttpPost]
        public ActionResult Sign(MethodsValues model)
        {
            using (var db = new MainDbContext())
            {
                var mv = db.MethodsValues.Create();
                mv.WorkerId = AMTSessionManager.GetWorkerId();
                mv.Method = PictureSessionManager.GetMethod().ToString();
                mv.Value = model.Value;
                db.MethodsValues.Add(mv);
                db.SaveChanges();
            }
            if (PictureSessionManager.GetMethod().Equals(ExperimentMethods.SignAtEnd))
            {
                return RedirectToAction("LastPage", "Summary");
            }
            return RedirectToAction("ManagePic", "Picture");

        }

        public ActionResult ReadParagraph()
        {
            PictureSessionManager.SetMethod(ExperimentMethods.RegularParagraph);
            ViewBag.Paragraph = GetParagraphString();
            return View();
        }


        private string GetParagraphString()
        {
            var s = "";
            switch (PictureSessionManager.GetMethod())
            {
                case ExperimentMethods.TenCommandments:
                    s = "Please choose one form the ten commandments";
                    break;
                case ExperimentMethods.RegularWriteParagraph:
                    s = "Please write a short paragraph about your favorite book.";
                    break;
                case ExperimentMethods.SignAtBeginning:
                    s = "By signing and clicking the agree button you promise to complete the tasks as requested.";
                    break;
                case ExperimentMethods.SignAtEnd:
                    s = "By signing and clicking the agree button you promise that you completed the tasks as requested.";
                    break;
                case ExperimentMethods.MoralParagraph:
                    s = "I call heaven and earth to witness against you this day," +
                        " that I have set before thee life and death, " +
                        "the blessing and the curse; therefore choose life, " +
                        "that thou mayest live, thou and thy seed.  " +
                        "to love the LORD thy God, to hearken to His voice, " +
                        "and to cleave unto Him; for that is thy life, and the length of thy days;" +
                        " that thou mayest dwell in the land which the LORD swore unto thy fathers, " +
                        "to Abraham, to Isaac, and to Jacob, to give them.";
                    break;
                case ExperimentMethods.RegularParagraph:
                    s = "Once upon a time, there was a little girl named Goldilocks.  " +
                        "She  went for a walk in the forest.  Pretty soon, she came upon a house.  " +
                        "She knocked and, when no one answered, she walked right in." +
                        "At the table in the kitchen, there were three bowls of porridge." +
                        "Goldilocks was hungry.She tasted the porridge from the first bowl."+
                        "'This porridge is too hot!' she exclaimed." +
                        "So, she tasted the porridge from the second bowl." +
                        "'This porridge is too cold', she said So, she tasted the last bowl of porridge." +
                        "'Ahhh, this porridge is just right', she said happily and she ate it all up.";
                    break;
                default:
                    break;
            }
            return s;
        }



    }
}