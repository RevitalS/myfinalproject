﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project1.Models;
using Project1.Utility;
using Project1.Constants;

namespace Project1.Controllers
{
    /// <summary>
    /// The short quiz.
    /// </summary>
    public class QuizController : Controller
    {
        [HttpGet]
        public ActionResult Quiz()
        {
            TimeSessionManager.StartQuizTimer();
            if (ExperimentSessionManager.GetState() > ExperimentState.Quiz)
            {
                return RedirectToAction("PictureInstructions", "Instructions");
            }
            ExperimentSessionManager.SetState(ExperimentState.Quiz);
            var questions = new Question();
            questions.AnswerOptions = getAnswersOptionsList();
            questions.QuizQuestions = getPicturesQuestionsList();

            return View(questions);
            
        }

        [HttpPost]
        public ActionResult Quiz(Question model)
        {
            var qustionModel = new Question();
            qustionModel.CorrectAnswers = getPictureAnswerList();  
            qustionModel.AnswerOptions = getAnswersOptionsList();

            if (model.userAns == null || model.userAns.Count()!= qustionModel.CorrectAnswers.Count())
            {
                    qustionModel.numOfCorrectAns = -1;
                qustionModel.QuizQuestions = getPicturesQuestionsList();
                ExperimentSessionManager.SetNumberOfTryQuiz();
                return View(qustionModel);
            }
            var correctAnsNum = 0;
            for (int i = 0; i < qustionModel.CorrectAnswers.Count(); i++)
            {
                if (model.userAns[i].Equals(qustionModel.CorrectAnswers.ElementAt(i)))
                {
                    correctAnsNum++;
                }
            }
            if (correctAnsNum < qustionModel.CorrectAnswers.Count())
            {
                qustionModel.numOfCorrectAns = correctAnsNum;
                qustionModel.QuizQuestions = getPicturesQuestionsList();
                qustionModel.userAns = model.userAns;
                ExperimentSessionManager.SetNumberOfTryQuiz();
                return View(qustionModel);
            }
            TimeSessionManager.StopQuizTimer();
            return RedirectToAction("UserProfile", "User");
        }


        private IEnumerable<string> getAnswersOptionsList()
        {
            var answerList = new List<string>()
            {
               "Yes", "No", "I Don't Know", "Yes", "No", "I Don't Know",
                "Yes - I need to choose the umbrella everyone thinks is the strongest",
                "No - I need to choose umbrella I thinks is the strongest", "I Don't Know"
            };
            return answerList;
        }

        private IEnumerable<string> getPicturesQuestionsList()
        {
            var question = new List<string>()
            {
                "Do I need to click on the links to see the images?",
                "Can I choose more than one image?",
                "Is there only one correct answer?"
            };
            return question;
        }

        private IEnumerable<string> getPictureAnswerList()
        {
            var answer = new List<string>()
            {
                "Yes",
                "No",
                "No - I need to choose umbrella I thinks is the strongest"
            };
            return answer;
        }

    }
}