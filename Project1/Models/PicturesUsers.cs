﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Project1.Models
{
    /// <summary>
    /// All the save data in the database.
    /// </summary>
    public class PicturesUsers
    {
        [Key]
        public string WorkerId { get; set; }

        [Required(ErrorMessage = "Please enter your age.")]
        public string Age { get; set; }
        virtual public IEnumerable<SelectListItem> Ages { get; set; }

        [Required(ErrorMessage = "Please enter your Gender.")]
        public string Gender { get; set; }
        virtual public IEnumerable<SelectListItem> Genders { get; set; }

        [Required(ErrorMessage = "Please enter your Country.")]
        public string Country { get; set; }
        virtual public IEnumerable<SelectListItem> Countries { get; set; }

        [Required(ErrorMessage = "Please enter your education.")]
        public string Education { get; set; }
        virtual public IEnumerable<SelectListItem> Educations { get; set; }
       
        public string ChosenImage { get; set; }

        public int? NumberOfLookedImages { get; set; }

        public Boolean CompleteViewImages { get; set; }

        public string Order { get; set; }

        public string StringOrder { get; set; }

        public string StringOpenPicturesOrder { get; set; }

        public string StringOpenPicturesTime { get; set; }

        public string StringTimeInPicture { get; set; }
        
        public string Method { get; set; }

        public string StartTime { get; set; }

        public int? InstructionsTime { get; set; }

        public int? TrainingTime { get; set; }

        public int? QuizTime { get; set; }

        public int? UserProfileTime { get; set; }

        public int? MethodTime { get; set; }

        public int? ViewImagesTime { get; set; }

        public int? ExperimentTime { get; set; }

        public string HitId { get; set; }
        public string AssignmentId { get; set; }

    }
}