﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Project1.Constants;

namespace Project1.Models
{
    /// <summary>
    /// Choose picture - getting data from view to pictures controller.
    /// </summary>
    public class ChosenPictures
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please select number.")]
        public string ChosenImage { get; set; }
        public int NumberOfPic { get; set; }
        public ExperimentMethods Method { get; set; }
        public bool? DidChoose { get; set; }
    }
}