﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.Models
{
    /// <summary>
    /// For sending question and getting answers from quiz view.
    /// </summary>
    public class Question
    {
        public IList<string> userAns { get; set; }

        virtual public IEnumerable<string> AnswerOptions { get; set; }

        virtual public IEnumerable<string> QuizQuestions { get; set; }

        virtual public IEnumerable<string> CorrectAnswers { get; set; }

        public int? numOfCorrectAns { get; set; }
    }
}