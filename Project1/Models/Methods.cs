﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project1.Models
{
    /// <summary>
    /// For save the methods and how many pepole need to do every method
    /// </summary>
    public class Methods
    {
        [Key]
        public string Id { get; set; }

        public int Counter { get; set; }
    }
}