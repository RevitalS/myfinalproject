﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project1.Models
{
    /// <summary>
    /// For save method value.
    /// </summary>
    public class MethodsValues
    {
        [Key]
        public string WorkerId { get; set; }

        public string Method { get; set; }
        public string Value { get; set; }
        virtual public IEnumerable ShowValue { get; set; }
    }
}