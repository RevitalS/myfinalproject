﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.ViewModel
{
    public class AMTModel
    {
        public bool IsAccepted { get; set; }
        public string AssignmentId { get; set; }
        public string HitId { get; set; }
        public string TurkSubmitToUrl { get; set; }
        public string WorkerId { get; set; }
        public double Bonus { get; set; }
        public string Payment { get; set; }
    }
}