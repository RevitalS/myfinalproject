﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Project1.Parsers
{
    public class OrdersParser
    {
        public static Dictionary<int, Tuple<string, string>> GetOrders()
        {
            var orderDict = new Dictionary<int, Tuple<string, string>>();
            using (var reader = new StreamReader (@"C:\Users\revit\OneDrive\Documents\Visual Studio 2017\Projects\Project1\Project1\Texts\Orders.csv"))
            {
                int rowNum = 0;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(",".ToArray(),2);
                    orderDict[rowNum] = new Tuple<string, string>(values[0], values[1]);
                    rowNum++;
                }

            }
            return orderDict;
        }
    }
}