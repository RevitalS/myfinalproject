﻿var seconds;
var interval;

function printClock() {
    if (seconds == -1) {
        clearInterval(interval);
        $("#allq").hide();
        $("#demo").hide();
        $("#end").show();
        return;
    }
    var min = Math.floor(seconds / 60)
    var sec = seconds % 60;
    var correctTime;
    if (sec > 9) {
        correctTime = min.toString() + ":" + sec.toString();
    } else {
        correctTime = min.toString() + ":0" + sec.toString();
    }
    $("#demo").html(correctTime);
    seconds--;
}

function startClock(totalSeconds) {
    seconds = totalSeconds;
    interval = setInterval(printClock, 1000);
}

startClock(120);