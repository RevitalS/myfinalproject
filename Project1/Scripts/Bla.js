﻿$(document).on("click", "#Button", function (e) {
    $('#Button').addClass("disabled");
    $('#Button').prop('disabled', true);
});

$(document).on('submit', 'form', function () {
    var button = $(this).find('button[type="submit"]');
    setTimeout(function () {
        button.attr('disabled', 'disabled');
    }, 0);
});

function printClock() {
    if (seconds === -1) {
        clearInterval(interval);
        $("#allq").hide();
        $("#demo").hide();
        $("#end").show();
        return;
    }
}