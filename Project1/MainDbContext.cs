﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Project1.Models;

namespace Project1
{
    public class MainDbContext : DbContext
    {
        public MainDbContext() : base("name=TryDB.mdf") { }

        public DbSet<PicturesUsers> PicturesUsers { get; set; }

        public DbSet<Orders> Orders { get; set; }
        public DbSet<Methods> Methods { get; set; }

        public DbSet<MethodsValues> MethodsValues { get; set; }
    }
}