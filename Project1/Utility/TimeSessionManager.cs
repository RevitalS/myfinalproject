﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.Utility
{
    public class TimeSessionManager
    {
        public static void StartGameTimer()
        {
            HttpContext.Current.Session["GameStartTime"] = DateTime.UtcNow;
        }
        public static void StopGameTimer()
        {
            HttpContext.Current.Session["GameStopTime"] = DateTime.UtcNow;
        }
        public static void StartInstructionsTimer()
        {
            HttpContext.Current.Session["InstructionsStartTime"] = DateTime.UtcNow;
        }
        public static void StopInstructionsTimer()
        {
            HttpContext.Current.Session["InstructionsStopTime"] = DateTime.UtcNow;
        }
        public static void StartTrainingTimer()
        {
            HttpContext.Current.Session["TrainingStartTime"] = DateTime.UtcNow;
        }
        public static void StopTrainingTimer()
        {
            HttpContext.Current.Session["TrainingStopTime"] = DateTime.UtcNow;
        }
        public static void StartQuizTimer()
        {
            HttpContext.Current.Session["QuizStartTime"] = DateTime.UtcNow;
        }
        public static void StopQuizTimer()
        {
            HttpContext.Current.Session["QuizStopTime"] = DateTime.UtcNow;
        }

        public static void StartUserProfileTimer()
        {
            HttpContext.Current.Session["UserProfileStartTime"] = DateTime.UtcNow;
        }
        public static void StopUserProfileTimer()
        {
            HttpContext.Current.Session["UserProfileStopTime"] = DateTime.UtcNow;
        }

            public static void StartMethodTimer()
        {
            HttpContext.Current.Session["MethodStartTime"] = DateTime.UtcNow;
        }
        public static void StopMethodTimer()
        {
            HttpContext.Current.Session["MethodStopTime"] = DateTime.UtcNow;
        }
        public static void StartViewPictureTimer()
        {
            HttpContext.Current.Session["ViewPictureStartTime"] = DateTime.UtcNow;
        }
        public static void StopViewPictureTimer()
        {
            HttpContext.Current.Session["ViewPictureStopTime"] = DateTime.UtcNow;
        }
        
        public static string GetStartTime()
        {
            return HttpContext.Current.Session["GameStartTime"].ToString();
        }

        public static int? GetGameTimer()
        {
            return GetTimerFromSessionInSeconds("GameStartTime", "GameStopTime");
        }
        public static int? GetViewPictureTimer()
        {
            return GetTimerFromSessionInSeconds("ViewPictureStartTime", "ViewPictureStopTime");
        }
        public static int? GetQuizTimer()
        {
            return GetTimerFromSessionInSeconds("QuizStartTime", "QuizStopTime");
        }
        public static int? GetInstructionsTimer()
        {
            return GetTimerFromSessionInSeconds("InstructionsStartTime", "InstructionsStopTime");
        }
        public static int? GetTrainingTimer()
        {
            return GetTimerFromSessionInSeconds("TrainingStartTime", "TrainingStopTime");
        }

        public static int? GetUserProfileTimer()
        {
            return GetTimerFromSessionInSeconds("UserProfileStartTime", "UserProfileStopTime");
        }

        public static int? GetMethodTimer()
        {
            return GetTimerFromSessionInSeconds("MethodStartTime", "MethodStopTime");
        }

        public static int? GetTimerFromSessionInSeconds(string startTimerSessionKey, string stopTimerSessionKey)
        {
            TimeSpan timeSpan;
            if (HttpContext.Current.Session[startTimerSessionKey] == null)
                return null;
            else if (HttpContext.Current.Session[stopTimerSessionKey] == null)
                timeSpan = TimeConverter.CalculateTimeSpan((DateTime)HttpContext.Current.Session[startTimerSessionKey], DateTime.UtcNow);
            else
                timeSpan = TimeConverter.CalculateTimeSpan((DateTime)HttpContext.Current.Session[startTimerSessionKey], (DateTime)HttpContext.Current.Session[stopTimerSessionKey]);
            return TimeConverter.ConvertTimeSpanToSeconds(timeSpan);
        }

    }
}