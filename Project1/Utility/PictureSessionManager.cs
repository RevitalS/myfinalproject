﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Project1.Constants;

namespace Project1.Utility
{
    public class PictureSessionManager
    {
        public static void SetClickList(string click)
        {
            string clickListString = GetClickListString();
            string timeInImageString = GetTimeInImageString();
            HttpContext.Current.Session["CloseImageTime"] = DateTime.UtcNow;
            if (timeInImageString == null)
            {
                timeInImageString = click + ":" + TimeSessionManager.GetTimerFromSessionInSeconds("PictureTime", "CloseImageTime").ToString();
            }
            else
            {
                timeInImageString += " , " + click + ":" + TimeSessionManager.GetTimerFromSessionInSeconds("PictureTime", "CloseImageTime").ToString();

            }
            HttpContext.Current.Session["TimeInImageString"] = timeInImageString;
            Dictionary<string, int?> clickList = HttpContext.Current.Session["ClickList"] as Dictionary<string, int?>;
            var order = GetOpenOrder();
            if (order.Equals(1)) {
                clickListString = order.ToString()+ ":" + click;
            }
            else
            {
                clickListString += " , " + order.ToString() + ":" + click;
            }
            HttpContext.Current.Session["ClickListString"] = clickListString;
            if (clickList.ContainsKey(click))
            {
                return;
            }
            clickList[click] = order;

            SetOpenOrder(order + 1);
            HttpContext.Current.Session["ClickList"] = clickList;
        }

        public static string GetTimeInImageString()
        {
            if (HttpContext.Current.Session["TimeInImageString"] == null)
            {
                return null;
            }
            return HttpContext.Current.Session["TimeInImageString"].ToString();
        }

        public static string GetClickListString()
        {
            if (HttpContext.Current.Session["ClickListString"] == null)
            {
                return null;
            }
            return HttpContext.Current.Session["ClickListString"].ToString();
        }

        public static Dictionary<string, int?> GetClickList()
        {
            if (HttpContext.Current.Session["ClickList"] == null)
                return null;
            return HttpContext.Current.Session["ClickList"] as Dictionary<string, int?>;
        }

        public static void InitClickList()
        {
            Dictionary<string, int?> clickList = new Dictionary<string, int?>();
            HttpContext.Current.Session["ClickList"] = clickList;
            SetOpenOrder(1);
        }

        public static void SetOpenPicureTime(string click)
        {

            string openImagesTimeString = GetOpenImagesTimeString();

            HttpContext.Current.Session["PictureTime"] = DateTime.UtcNow;
            if (openImagesTimeString == null)
            {
                openImagesTimeString = click + ":" + TimeSessionManager.GetTimerFromSessionInSeconds("ViewPictureStartTime", "PictureTime").ToString();
            }
            else
            {
                openImagesTimeString += " , " + click + ":" + TimeSessionManager.GetTimerFromSessionInSeconds("ViewPictureStartTime", "PictureTime").ToString();
            }
            HttpContext.Current.Session["OpenImagesTimeString"] = openImagesTimeString;
            Dictionary<string, int?> timeList = 
                HttpContext.Current.Session["PicturesTime"] as Dictionary<string, int?>;
            //Keep the first time click on page.
            if (timeList.ContainsKey(click))
            {
                return;
            }

            timeList[click] = TimeSessionManager.GetTimerFromSessionInSeconds("ViewPictureStartTime", "PictureTime");
            HttpContext.Current.Session["PicturesTime"] = timeList;
        }

        public static string GetOpenImagesTimeString()
        {
            if (HttpContext.Current.Session["OpenImagesTimeString"] == null)
            {
                return null;
            }
            return HttpContext.Current.Session["OpenImagesTimeString"].ToString();
        }

        public static Dictionary<string, int?> GetOpenPictureTime()
        {
            if (HttpContext.Current.Session["PicturesTime"] == null)
                return null;
            return HttpContext.Current.Session["PicturesTime"] as Dictionary<string, int?>;
        }

        public static void InitPictureTimeList()
        {
            Dictionary<string, int?> listTime = new Dictionary<string, int?>();
            HttpContext.Current.Session["PicturesTime"] = listTime;
        }

        public static int GetOpenOrder()
        {
            return int.Parse(HttpContext.Current.Session["OpenOrder"].ToString());
        }
        public static void SetOpenOrder(int openOrder)
        {
            HttpContext.Current.Session["OpenOrder"] = openOrder;
        }


        public static ExperimentMethods? GetMethod()
        {
            if (HttpContext.Current.Session["Method"] == null)
                return null;
            return (ExperimentMethods)Enum.Parse(typeof(ExperimentMethods), HttpContext.Current.Session["Method"].ToString());
        }
        public static void SetMethod(ExperimentMethods method)
        {
            HttpContext.Current.Session["Method"] = method;
        }

        public static int? GetFlag()
        {
            if (HttpContext.Current.Session["Flag"] == null)
            {
                return null;
            }
            return int.Parse(HttpContext.Current.Session["Flag"].ToString());
        }

        public static void SetFlag(int flag)
        {
            HttpContext.Current.Session["Flag"] = flag;
        }
    }
}