﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Project1.Constants;

namespace Project1.Utility
{
    public class ExperimentSessionManager
    {
        public static string GetExperimentKind()
        {
            if (HttpContext.Current.Session["ExperimentKind"] == null)
                return null;
            return HttpContext.Current.Session["ExperimentKind"].ToString();
        }
        public static void SetExperimentKind(string experimentKind)
        {
            HttpContext.Current.Session["ExperimentKind"] = experimentKind;
        }

        public static ExperimentState? GetState()
        {
            if (HttpContext.Current.Session["State"] == null)
                return null;
            return (ExperimentState)Enum.Parse(typeof(ExperimentState), HttpContext.Current.Session["State"].ToString());
        }

        public static void SetState(ExperimentState state)
        {
            HttpContext.Current.Session["State"] = state;
        }

        public static string GetMethod()
        {
            if (HttpContext.Current.Session["Method"] == null)
                return null;
            return HttpContext.Current.Session["Method"].ToString();
        }
        public static void SetWorkerId(string method)
        {
            HttpContext.Current.Session["Method"] = method;
        }

        public static int? GetNumberOfTryQuiz()
        {
            if (HttpContext.Current.Session["QuizTry"] == null)
            {
                return null;
            }
            return int.Parse(HttpContext.Current.Session["QuizTry"].ToString());
        }

        public static void SetNumberOfTryQuiz()
        {
            if (HttpContext.Current.Session["QuizTry"] == null)
            {
                HttpContext.Current.Session["QuizTry"] = 1;
            }
            else
            {
                var num =  int.Parse(HttpContext.Current.Session["QuizTry"].ToString());
                num++;
                HttpContext.Current.Session["QuizTry"] = num;
            }
        }
    }
}