﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.Utility
{
    public class AMTSessionManager
    {
        public static string GetWorkerId()
        {
            if (HttpContext.Current.Session["WorkerId"] == null)
                return null;
            return HttpContext.Current.Session["WorkerId"].ToString();
        }
        public static void SetWorkerId(string workerId)
        {
            HttpContext.Current.Session["WorkerId"] = workerId;
        }
        public static string GetTurkSubmitTo()
        {
            if (HttpContext.Current.Session["TurkSubmitTo"] == null)
                return null;
            return HttpContext.Current.Session["TurkSubmitTo"].ToString();
        }
        public static void SetTurkSubmitTo(string turkSubmitTo)
        {
            HttpContext.Current.Session["TurkSubmitTo"] = turkSubmitTo;
        }
        public static string GetHitId()
        {
            if (HttpContext.Current.Session["HitId"] == null)
                return null;
            return HttpContext.Current.Session["HitId"].ToString();
        }
        public static void SetHitId(string hitId)
        {
            HttpContext.Current.Session["HitId"] = hitId;
        }
        public static string GetAssignmentId()
        {
            if (HttpContext.Current.Session["AssignmentId"] == null)
                return null;
            return HttpContext.Current.Session["AssignmentId"].ToString();
        }
        public static void SetAssignmentId(string assignmentId)
        {
            HttpContext.Current.Session["AssignmentId"] = assignmentId;
        }
    }
}