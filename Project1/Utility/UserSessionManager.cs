﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.Utility
{
    public class UserSessionManager
    {
        public static void SetCurrentScore(double score)
        {
            HttpContext.Current.Session["CurrentScore"] = score;
        }
        public static double? GetCurrentScore()
        {
            if (HttpContext.Current.Session["CurrentScore"] == null)
                return null;
            return double.Parse(HttpContext.Current.Session["CurrentScore"].ToString());
        }
        public static void SetCurrentBonus(double moneyRevenue)
        {
            HttpContext.Current.Session["CurrentBonus"] = moneyRevenue;
        }
        public static double? GetCurrentBonus()
        {
            if (HttpContext.Current.Session["CurrentBonus"] == null)
                return null;
            return double.Parse(HttpContext.Current.Session["CurrentBonus"].ToString());
        }
        public static List<int> GetUserEntriesIds()
        {
            if (HttpContext.Current.Session["UserEntriesIds"] == null)
                return null;
            return HttpContext.Current.Session["UserEntriesIds"] as List<int>;
        }
        public static void SetUserEntriesIds(List<int> entriesIds)
        {
            HttpContext.Current.Session["UserEntriesIds"] = entriesIds;
        }
    }
}