﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.Utility
{
    public class TimeConverter
    {
        public static TimeSpan CalculateTimeSpan(DateTime startTime, DateTime endTime)
        {
            return (endTime - startTime);
        }
        public static int ConvertTimeSpanToSeconds(TimeSpan timeSpan)
        {
            return (int)timeSpan.TotalSeconds; ;
        }
    }
}