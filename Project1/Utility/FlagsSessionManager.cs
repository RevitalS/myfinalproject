﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.Utility
{
    public class FlagsSessionManager
    {

        public static bool? GetUserFlag()
        {
            if (HttpContext.Current.Session["UserFlag"] == null)
            {
                return null;
            }
            return bool.Parse(HttpContext.Current.Session["UserFlag"].ToString());
        }

        public static void SetUserFlag(bool flag)
        {
            HttpContext.Current.Session["UserFlag"] = flag;
        }

    }
}