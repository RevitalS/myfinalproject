﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Project1.Constants;
    using System.Data.Entity;

    namespace Project1
    {
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Session_End()
        {
            if ((Session["State"] == null) || ((ExperimentState)Enum.Parse(typeof(ExperimentState), Session["State"].ToString()) != ExperimentState.Completed))
            {
                ClearUserData();
            }
        }


        /*       private void ClearEntry()
               {
                   List<int> entryListIds;
                   if (Session["UserEntriesIds"] != null)
                   {
                       entryListIds = Session["UserEntriesIds"] as List<int>;
                       using (var db = new ExpirimentDbContext())
                       {
                           foreach (int id in entryListIds)
                           {
                               var entry = db.Entries.Where(e => e.Id == id).FirstOrDefault();
                               if (entry.UsedCount > 0)
                               {
                                   entry.UsedCount--;
                                   db.Entry(entry).State = EntityState.Modified;
                                   db.SaveChanges();
                               }
                           }
                       }

                   }
               }*/
        private void ClearUserData()
        {
            string workerId;
            using (var db = new MainDbContext())
            {
                if (Session["WorkerId"] != null)
                {
                    workerId = Session["WorkerId"].ToString();

                    var user = db.PicturesUsers.Where(e => e.WorkerId.Equals(workerId)).FirstOrDefault();
                    if (user != null)
                    {
                        db.PicturesUsers.Remove(user);
                        db.SaveChanges();
                    }

                    var mv = db.MethodsValues.Where(e => e.WorkerId.Equals(workerId)).FirstOrDefault();
                    if (mv != null)
                    {
                        db.MethodsValues.Remove(mv);
                        db.SaveChanges();
                    }
                }
                    //set order
                    /*var orderNum = Session["Order"].ToString();
                    var o = db.Orders.Where(e => e.Name.Equals(orderNum)).FirstOrDefault().Counter;
                    o++;
                    db.Orders.Where(e => e.Name.Equals(orderNum)).FirstOrDefault().Counter = o;
                    db.SaveChanges();
                    */
                    //set method
                    if (Session["Method"] != null)
                    {
                        var methodNum = Session["Method"].ToString();
                        var o = db.Methods.Where(e => e.Id.Equals(methodNum)).FirstOrDefault().Counter;
                        o++;
                        db.Methods.Where(e => e.Id.Equals(methodNum)).FirstOrDefault().Counter = o;
                        db.SaveChanges();
                    }
                

            }

        }
    }
}
