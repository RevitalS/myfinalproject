﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.Constants
{
    /// <summary>
    /// Eunm - for the methods
    /// </summary>
    public enum ExperimentMethods
    {
        None = 1,
        TenCommandments = 2,
        FakePage = 3,
        RegularWriteParagraph = 4,
        Eyes = 5,
        SignAtBeginning = 6,
        SignAtEnd = 7,
        MoralPicture = 8,
        MoralParagraph = 9,
        RegularParagraph = 10,
        SignByRecord = 11,
        SignByListen = 12,
        SignByRead = 13
    }
}