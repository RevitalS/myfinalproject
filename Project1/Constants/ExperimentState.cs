﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.Constants
{
    /// <summary>
    /// The Experiment state - to know which state the user now.
    /// </summary>
    public enum ExperimentState
    {
        Instructions = 0,
        Training = 1,
        Quiz = 2,
        PersonalInfo = 3,
        Experiment = 4,
        InPicture = 5,
        Completed = 6
    }
}