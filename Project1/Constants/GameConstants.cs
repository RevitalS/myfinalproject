﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project1.Constants
{
    /// <summary>
    /// All the constant in the game.
    /// </summary>
    public static class GameConstants
    {
        public const int ParticipantsNumber = 100;
        public const int NumberOfOrders = 0;
        public const int NumberOfMethods = 3;
        public const int Payment = 50;
        public const int NumberOfPictures = 30;
    }
}